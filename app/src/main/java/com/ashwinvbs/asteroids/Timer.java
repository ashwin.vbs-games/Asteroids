package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import java.util.List;

public abstract class Timer extends Actor {
    public enum Type {
        ONCE,
        PERIODIC,
        PERIODIC_DELAYED
    }

    private float timeSinceLastTrigger = 0f;
    protected float delay = 0f;
    private Type type;

    Timer(float delay, Type type){
        this.delay = delay;
        this.type = type;
        if(type == Type.PERIODIC) timeSinceLastTrigger = delay + 1;
    }

    @Override
    public void draw(Canvas canvas) {

    }

    @Override
    public void checkPhysics(List<Actor> objects, List<Actor> delta) {

    }

    public abstract void run(AsteroidGame.SensorInfo sensor, List<Actor> objects);

    private void setDelay(float delay){
        this.delay = delay;
    }

    @Override
    public void tick(float delta, AsteroidGame.SensorInfo sensor, List<Actor> objects) {
        timeSinceLastTrigger += delta;
        if(timeSinceLastTrigger > delay){
            run(sensor, objects);
            if(this.type == Type.ONCE)
                objects.add(this);
            else if(this.type == Type.PERIODIC)
                timeSinceLastTrigger = 0f;
        }
    }
}
