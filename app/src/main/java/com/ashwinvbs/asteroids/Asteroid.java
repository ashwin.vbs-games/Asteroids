package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.List;

public class Asteroid extends Projectile {
    private Paint paint;

    AsteroidGame.ScreenInfo screen;
    float radius;

    public Asteroid(AsteroidGame.ScreenInfo screenInfo, Utility.Vector2D pos, Utility.Vector2D vel, float rRadius){
        super(screenInfo.aspect, pos, vel, Measurements.MaxSpeedAsteroid);
        screen = screenInfo;
        radius = rRadius;
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void drawInstance(Canvas canvas, Point offset){
        Utility.Vector2D center = Utility.offsetPoint(super.position, screen.aspect, offset);

        canvas.drawCircle(center.x * screen.scale, center.y * screen.scale, radius * screen.scale, paint);
    }

    protected void breakup(Utility.Vector2D direction, List<Actor> delta){
        delta.add(this);
        if(radius > Measurements.MeteorRadius/4f){
            Utility.Vector2D deltaVel = direction.normalize().scale(velocity.length());

            Utility.Vector2D vec1 = Utility.Vector2D.random();
            delta.add(new Asteroid(screen, position.offset(vec1.scale(radius/2)), velocity.offset(deltaVel.offset(vec1.scale(velocity.length()/2f))), radius/2f ));

            vec1 = Utility.Vector2D.random();
            delta.add(new Asteroid(screen, position.offset(vec1.scale(radius/2)), velocity.offset(deltaVel.offset(vec1.scale(velocity.length()/2f))), radius/2f ));

            if(Math.random() > 0.5f) {
                vec1 = Utility.Vector2D.random();
                delta.add(new Asteroid(screen, position.offset(vec1.scale(radius / 2)), velocity.offset(deltaVel.offset(vec1.scale(velocity.length() / 2f))), radius / 2f));
            }
        }
    }

    public void checkPhysics(List<Actor> objects, List<Actor> delta){
        for(Actor a : objects){
            if(a instanceof Bullet) {
                Bullet b = (Bullet) a;
                Utility.Vector2D end = b.prevPosition.offset(b.traversed);
                Utility.Vector2D start = b.position.offset(b.traversed.scale(-1f));
                if (Utility.checkIntersection(super.position, radius, start, b.position) || Utility.checkIntersection(super.position, radius, b.prevPosition, end)){
                    this.breakup(b.velocity, delta);
                    delta.add(b);
                }
            }
            else if(a instanceof Ship){
                Ship ship = (Ship) a;
                if( Utility.checkIntersection(super.position, radius, ship.front(), ship.right()) ||
                    Utility.checkIntersection(super.position, radius, ship.front(), ship.left()) ||
                    Utility.checkIntersection(super.position, radius, ship.left(), ship.right()) )
                    delta.add(a);
            }
        }
    }
}
