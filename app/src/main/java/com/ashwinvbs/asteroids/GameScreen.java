package com.ashwinvbs.asteroids;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.Window;
import android.view.WindowManager;

public class GameScreen extends AppCompatActivity implements SensorEventListener {

    private AsteroidGame game;
    private GameView view;
    private GameScreen activity;
    private Thread loop;

    private SensorManager mgr;
    private Sensor gravity;

    private Display display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        //assert size.x > size.y;

        AsteroidGame.ScreenInfo screen = new AsteroidGame.ScreenInfo();
        screen.size = size;
        screen.aspect = (float)size.x/(float)size.y;
        screen.scale = ((float)size.y)/100f;

        game = new AsteroidGame(screen);
        view = new GameView(this, game);
        activity = this;
        setContentView(view);

        mgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        gravity = mgr.getDefaultSensor(Sensor.TYPE_GRAVITY);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mgr.registerListener(this, gravity, 10000);
        game.unpause();
        loop = new Thread(new Runnable(){
            public void run(){
                while(!Thread.interrupted()){
                    if(!game.isRunning()) {
                        if (game.wasKilled())
                            return;
                        else {
                            activity.onEndGame();
                            activity.finish();
                        }
                    }
                    game.tick();
                    activity.runOnUiThread(new Runnable(){
                        public void run(){
                            view.invalidate();
                        }
                    });
                    try{
                        Thread.sleep(30, 0);
                    }
                    catch(InterruptedException e){
                        return;
                    }
                }
            }
        });
        loop.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mgr.unregisterListener(this, gravity);
        loop.interrupt();
        game.pause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getPointerCount() == 1){
            int action = MotionEventCompat.getActionMasked(event);
            switch(action){
                case MotionEvent.ACTION_UP:
                    game.onThumbUp();
                    break;
                case MotionEvent.ACTION_DOWN:
                    game.onThumbDown();
                    break;
            }
        }
        game.onTarget(new Utility.Vector2D(event.getX(), event.getY()));
        return true;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double rotation = 0D;
        switch(display.getRotation()){
            case Surface.ROTATION_0:
                rotation = Math.toRadians(0F);
                break;
            case Surface.ROTATION_90:
                rotation = Math.toRadians(90F);
                break;
            case Surface.ROTATION_180:
                rotation = Math.toRadians(180F);
                break;
            case Surface.ROTATION_270:
                rotation = Math.toRadians(270F);
                break;
        }

        Utility.Vector2D gravity = new Utility.Vector2D(event.values[0], event.values[1]).scale(1f/9.8f).rotate(rotation);

        game.onTilt(gravity);
    }

    @Override
    public void onBackPressed() {
        game.forceKill();
        onEndGame();
        super.onBackPressed();
    }

    public void onEndGame(){
        Intent data = new Intent();
        data.putExtra("com.ashwinvbs.asteroids.time", game.getRuntime());
        setResult(RESULT_OK, data);
    }
}
