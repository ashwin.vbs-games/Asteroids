package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Handler;

import java.util.List;

public class AsteroidGame {
    public static class ScreenInfo{
        public Point size;
        public float scale;
        public float aspect;
    }
    private ScreenInfo screen;

    public static class SensorInfo{
        public Utility.Vector2D tilt;
        public long downTime, upTime;
        public boolean click;
        public Utility.Vector2D target;
    }
    private SensorInfo sensor = new SensorInfo();

    private boolean running = true;
    private boolean waskilled = false;

    public static class FrameInfo{
        public long prevTick, latestTick;
        public float totalTime = 0f;
        public boolean paused = false;
    }
    private FrameInfo clock = new FrameInfo();

    private SceneGraph graph;

    AsteroidGame(ScreenInfo info){
        screen = info;
        clock.prevTick = clock.latestTick = System.currentTimeMillis();
        sensor.downTime = sensor.upTime = 0L;
        sensor.tilt = new Utility.Vector2D();

        final Ship ship = new Ship(screen);
        graph = new SceneGraph(this, ship);
        graph.objects.add(new Timer(10, Timer.Type.PERIODIC){
            @Override
            public void run(SensorInfo sensor, List<Actor> objs) {
                Utility.Vector2D position = ship.position.offset(new Utility.Vector2D(50f*screen.aspect, 50f)).offset(new Utility.Vector2D(25f*screen.aspect*(float)Math.random(), 25f*(float)Math.random()));
                Utility.Vector2D velocity = Utility.Vector2D.random().scale(Measurements.MaxSpeedAsteroid/3f);

                objs.add(new Asteroid(screen, position, velocity, Measurements.MeteorRadius));

                delay = delay*0.95f;
            }
        });
    }

    //game ticks
    public void tick(){
        if(!clock.paused) {
            sensor.click = (sensor.downTime > clock.prevTick) || (sensor.downTime > sensor.upTime);
            if (!(clock.latestTick > clock.prevTick)) {
                clock.latestTick = System.currentTimeMillis();
            }

            long millis = clock.latestTick - clock.prevTick;

            graph.tick(((float) millis) / 1000f, sensor);

            clock.totalTime += (float) millis / 1000f;

            clock.prevTick = clock.latestTick;
        }
    }

    public void onDraw(Canvas canvas){
        canvas.drawColor(Color.BLACK);
        if(false){
            if(sensor.click)
                canvas.drawColor(Color.RED);

            Paint paint = new Paint();
            paint.setColor(Color.BLUE);

            canvas.drawLine(    screen.size.x/2,
                                screen.size.y/2,
                                screen.size.x/2 + sensor.tilt.x*((float)screen.size.y)/2,
                                screen.size.y/2 + sensor.tilt.y*((float)screen.size.y)/2, paint);
        }

        graph.draw(canvas);
    }

    //game pause event
    public void pause(){
        clock.latestTick = System.currentTimeMillis();
        clock.paused = true;
    }

    public void unpause(){
        clock.paused = false;
    }

    //setters
    public void onTilt(Utility.Vector2D tilt){
        sensor.tilt = tilt;
    }

    public void onThumbDown(){
        sensor.downTime = System.currentTimeMillis();
    }

    public void onThumbUp(){
        sensor.upTime = System.currentTimeMillis();
    }

    public void onTarget(Utility.Vector2D target) { sensor.target = target; }

    public void onGameOver(){
        pause();
        try{
            Thread.sleep(1000, 0);
        }
        catch(InterruptedException e){
            running = false;
            return;
        }
        running = false;
    }

    public void forceKill(){
        pause();
        running = false;
        waskilled = true;
    }

    public float getRuntime(){ return clock.totalTime; }

    public boolean isRunning(){return running;}

    public boolean wasKilled(){return waskilled;}
}
