package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.List;

public class Bullet extends Projectile {
    AsteroidGame.ScreenInfo screen;
    private Paint paint;
    private float life = 0f;

    public Bullet(AsteroidGame.ScreenInfo screenInfo, Utility.Vector2D pos, Utility.Vector2D vel){
        super(screenInfo.aspect, pos, vel.scale(Measurements.SpeedBullet/vel.length()), Measurements.SpeedBullet);
        screen = screenInfo;
        paint = new Paint();
        paint.setColor(Color.WHITE);
    }

    @Override
    public void tick(float delta, AsteroidGame.SensorInfo sensor, List<Actor> objects) {
        super.tick(delta, sensor, objects);
        life += maxSpeed * delta;
        if(life > Measurements.BulletLifetime)
            objects.add(this);
    }

    public void checkPhysics(List<Actor> objects, List<Actor> delta){
    }

    @Override
    protected void drawInstance(Canvas canvas, Point offset) {
        Utility.Vector2D start = Utility.offsetPoint(position, screen.aspect, offset);
        Utility.Vector2D end = start.offset(velocity.normalize().scale(4f));

        canvas.drawLine(start.x * screen.scale, start.y * screen.scale, end.x * screen.scale, end.y * screen.scale, paint);
    }
}
