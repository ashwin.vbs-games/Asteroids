package com.ashwinvbs.asteroids;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class KillView extends View{
    private AsteroidGame.ScreenInfo screen;

    private Paint titlePaint = new Paint();
    private Paint scorePaint = new Paint();
    private Paint instrPaint = new Paint();

    private KillScreen.GameSummary summary;

    KillView(Context context, KillScreen.GameSummary summary, AsteroidGame.ScreenInfo screen){
        super(context);
        this.screen = screen;
        this.summary = summary;

        titlePaint.setTextAlign(Paint.Align.CENTER);
        scorePaint.setTextAlign(Paint.Align.CENTER);
        instrPaint.setTextAlign(Paint.Align.CENTER);

        titlePaint.setColor(Color.WHITE);
        scorePaint.setColor(Color.WHITE);
        instrPaint.setColor(Color.WHITE);

        titlePaint.setTextSize(screen.size.y/4);
        scorePaint.setTextSize(screen.size.y/16);
        instrPaint.setTextSize(screen.size.y/16);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.BLACK);

        canvas.drawText("Asteroids", screen.size.x/2, screen.size.y/3, titlePaint);
        canvas.drawText("Press and hold to start a new game.", screen.size.x/2, 15*screen.size.y/16, instrPaint);

        if(summary.time > 0)
            canvas.drawText("You lasted " + (int)(summary.time * 1000) + " milli seconds.", screen.size.x/2, 3*screen.size.y/4, scorePaint);
    }
}
