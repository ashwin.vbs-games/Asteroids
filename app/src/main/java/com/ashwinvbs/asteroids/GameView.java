package com.ashwinvbs.asteroids;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

public class GameView extends View {
    private AsteroidGame game;

    GameView(Context context, AsteroidGame AGame){
        super(context);
        game = AGame;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        game.onDraw(canvas);
    }
}
