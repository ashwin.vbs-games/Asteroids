package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SceneGraph {
    public List<Actor> objects;
    private AsteroidGame game;
    private Actor star;

    private final Lock mutex = new ReentrantLock(true);

    SceneGraph(AsteroidGame game, Actor star){
        objects = new LinkedList<Actor>();
        this.game = game;
        this.star = star;
        objects.add(star);
    }

    public void tick(float delta, AsteroidGame.SensorInfo sensor){
        mutex.lock();

        List<Actor> addendum = new LinkedList<Actor>();

        for(Actor a : objects){
            a.tick(delta, sensor, addendum);
        }

        for(Actor a : objects){
            a.checkPhysics(objects, addendum);
        }

        processAddendum(addendum);

        mutex.unlock();
    }

    private void processAddendum(List<Actor> addendum){
        if(addendum.contains(star)) {
            game.onGameOver();
            return;
        }

        for(Actor b : addendum){
            if(objects.contains(b))
                objects.remove(b);
            else
                objects.add(b);
        }
    }

    public void draw(Canvas canvas){
        mutex.lock();
        for(Actor a : objects){
            a.draw(canvas);
        }
        mutex.unlock();
    }
}
