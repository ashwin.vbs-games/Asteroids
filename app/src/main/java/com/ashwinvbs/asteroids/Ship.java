package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import java.util.List;

public class Ship extends Projectile {
    AsteroidGame.ScreenInfo screen;
    private float orientation = 0f;
    private Paint paint;
    private float timeSinceLastBullet = 100f;

    public Ship(AsteroidGame.ScreenInfo screenInfo){
        super(screenInfo.aspect, new Utility.Vector2D(50 * screenInfo.aspect, 50), new Utility.Vector2D(0,0), Measurements.MaxSpeedShip);
        screen = screenInfo;
        paint = new Paint();
        paint.setColor(Color.WHITE);
    }

    public Utility.Vector2D front(){
        Utility.Vector2D front = Measurements.ShipFront.rotate(orientation);
        return position.offset(front);
    }

    public Utility.Vector2D right(){
        Utility.Vector2D right = Measurements.ShipRight.rotate(orientation);
        return position.offset(right);
    }

    public Utility.Vector2D left(){
        Utility.Vector2D left = Measurements.ShipLeft.rotate(orientation);
        return position.offset(left);
    }

    @Override
    protected void drawInstance(Canvas canvas, Point offset) {

        Utility.Vector2D center = Utility.offsetPoint(position, screen.aspect, offset);

        Utility.Vector2D front = Measurements.ShipFront.rotate(orientation);
        Utility.Vector2D left = Measurements.ShipLeft.rotate(orientation);
        Utility.Vector2D right = Measurements.ShipRight.rotate(orientation);

        front = center.offset(front).scale(screen.scale);
        left = center.offset(left).scale(screen.scale);
        right = center.offset(right).scale(screen.scale);

        canvas.drawLine(front.x, front.y, left.x, left.y, paint);
        canvas.drawLine(right.x, right.y, left.x, left.y, paint);
        canvas.drawLine(front.x, front.y, right.x, right.y, paint);
    }

    private void spawnBullet(Utility.Vector2D target, List<Actor> objects){
        Bullet b = new Bullet(screen, position.copy(), target.offset(position.scale(-1f)));
        objects.add(b);
    }

    @Override
    public void tick(float delta, AsteroidGame.SensorInfo sensor, List<Actor> objects) {
        super.tick(delta, sensor, objects);
        if(sensor.click) {
            orientation = -1 * (float) Math.atan2(sensor.target.y / screen.scale - this.position.y, sensor.target.x / screen.scale - this.position.x);
            if(timeSinceLastBullet < Measurements.BulletInterval)
                timeSinceLastBullet += delta;
            else {
                spawnBullet(sensor.target.scale(1f/screen.scale), objects);
                timeSinceLastBullet = 0f;
            }
        } else {
            timeSinceLastBullet = 100f;
        }
        super.acceleration = sensor.tilt;
    }

    @Override
    public void checkPhysics(List<Actor> objects, List<Actor> delta){
    }
}
