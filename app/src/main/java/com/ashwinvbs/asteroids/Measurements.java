package com.ashwinvbs.asteroids;

public abstract class Measurements {
    public static float MeteorRadius = 8F;
    public static Utility.Vector2D ShipFront = new Utility.Vector2D(-5F, 0F);
    public static Utility.Vector2D ShipLeft = new Utility.Vector2D(1.25F, 2.165F);
    public static Utility.Vector2D ShipRight = new Utility.Vector2D(1.25F, -2.165F);
    public static float MaxSpeedShip = 30F;
    public static float SpeedBullet = 150F;
    public static float MaxSpeedAsteroid = 50F;
    public static float accelerationScaling = 200F;
    public static float BulletInterval = 0.2f;
    public static float BulletLifetime = 200f;
}
