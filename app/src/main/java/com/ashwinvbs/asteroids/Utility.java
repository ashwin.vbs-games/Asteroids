package com.ashwinvbs.asteroids;

import android.graphics.Point;

public abstract class Utility {
    public static Vector2D offsetPoint(Vector2D vec, float aspect, Point offset){
        Vector2D vectorOffset = new Vector2D(offset.x * aspect * 100f, offset.y * 100f);
        return vec.offset(vectorOffset);
    }

    public static boolean checkIntersection(Vector2D center, float radius, Vector2D start, Vector2D end){
        Vector2D k = start.offset(center.scale(-1f));
        Vector2D v = end.offset(start.scale(-1f));
        float r = radius;

        float discriminant = (4 * k.dot(v) * k.dot(v)) - (4 * v.dot(v) * (k.dot(k) - (r * r)));
        if(discriminant < 0)
            return false;
        discriminant = (float)Math.sqrt(discriminant);
        float d1 = (discriminant - (2*k.dot(v)))/(2*v.dot(v));
        float d2 = ((-1f*discriminant) - (2*k.dot(v)))/(2*v.dot(v));
        if(d1 < 1f && d1 > 0f)
            return true;
        else if(d2 < 1f && d2 > 0f)
            return true;
        else
            return false;
    }

    static class Vector2D{
        public float x, y;

        public Vector2D(){
            x = y = 0f;
        }

        public Vector2D(float x, float y){
            this.x = x;
            this.y = y;
        }

        public static Vector2D random(){
            return new Vector2D((float)(Math.random()*2 - 1), (float)(Math.random()*2 - 1)).normalize();
        }

        public Vector2D rotate(double angle){
            double xDash = 0D - x*Math.cos(angle) + y*Math.sin(angle);
            double yDash = x*Math.sin(angle) + y*Math.cos(angle);

            return new Vector2D((float)xDash, (float)yDash);
        }

        public Vector2D scale(float factor){
            return new Vector2D(factor*x, factor*y);
        }

        public float dot(Vector2D second){
            return second.x*this.x + second.y*this.y;
        }

        public Vector2D offset(Vector2D delta){
            return new Vector2D(x + delta.x, y + delta.y);
        }

        public float lengthSquared(){
            return x*x + y*y;
        }

        public float length(){ return (float)Math.sqrt(lengthSquared()); }

        public Vector2D normalize(){
            return scale(1f/length());
        }

        public Vector2D copy() {return new Vector2D(x, y); }
    }
}
