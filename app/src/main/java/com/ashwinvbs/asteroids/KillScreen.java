package com.ashwinvbs.asteroids;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class KillScreen extends AppCompatActivity {

    private KillView view;
    private Display display;

    private KillScreen activity;

    public class GameSummary{
        public float time;
    }

    private GameSummary summary = new GameSummary();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        //assert size.x > size.y;

        AsteroidGame.ScreenInfo screen = new AsteroidGame.ScreenInfo();
        screen.size = size;
        screen.aspect = (float)size.x/(float)size.y;
        screen.scale = ((float)size.y)/100f;

        view = new KillView(this, summary, screen);
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                activity.startGame();
                return true;
            }
        });
        setContentView(view);
    }

    public void startGame(){
        Intent intent = new Intent(this, GameScreen.class);
        startActivityForResult(intent, 999);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 999 && resultCode == RESULT_OK){
            summary.time = data.getFloatExtra("com.ashwinvbs.asteroids.time", -1f);
            view.invalidate();
        }
    }

}
