package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import android.graphics.Point;
import java.util.List;

public abstract class Projectile extends Actor{
    protected float aspectRatio = 1F;

    protected Utility.Vector2D velocity;
    protected Utility.Vector2D position;
    protected Utility.Vector2D acceleration = new Utility.Vector2D();

    protected Utility.Vector2D traversed = new Utility.Vector2D(0, 0);
    protected Utility.Vector2D prevPosition = new Utility.Vector2D(0, 0);

    protected float maxSpeed;

    public Projectile(float aspect, Utility.Vector2D pos, Utility.Vector2D vel, float maxSpeed){
        aspectRatio  = aspect;
        velocity = vel;
        position = pos;
        this.maxSpeed = maxSpeed;
    }

    public void tick(float delta, AsteroidGame.SensorInfo sensor, List<Actor> objects){
        velocity = velocity.offset(acceleration.scale(delta*Measurements.accelerationScaling));

        float speed = velocity.length();

        if(speed > maxSpeed)
            velocity = velocity.scale(maxSpeed/speed);

        //store deltas for physics calculation
        prevPosition = position.copy();

        traversed = velocity.scale(delta);

        position = position.offset(traversed);

        if(position.x > 100F*aspectRatio) position.x -= 100F*aspectRatio;
        if(position.x < 0F) position.x += 100F*aspectRatio;

        if(position.y > 100F) position.y -= 100F;
        if(position.y < 0F) position.y += 100F;
    }

    protected abstract void drawInstance(Canvas canvas, Point offset);

    public void draw(Canvas canvas){
        drawInstance(canvas, new Point(-1, +1));   drawInstance(canvas, new Point( 0, +1));   drawInstance(canvas, new Point(+1, +1));
        drawInstance(canvas, new Point(-1,  0));   drawInstance(canvas, new Point( 0,  0));   drawInstance(canvas, new Point(+1,  0));
        drawInstance(canvas, new Point(-1, -1));   drawInstance(canvas, new Point( 0, -1));   drawInstance(canvas, new Point(+1, -1));
    }
}
