package com.ashwinvbs.asteroids;

import android.graphics.Canvas;
import java.util.List;

public abstract class Actor {
    public abstract void tick(float delta, AsteroidGame.SensorInfo sensor, List<Actor> objects);
    public abstract void draw(Canvas canvas);
    public abstract void checkPhysics(List<Actor> objects, List<Actor> delta);
}
